# coding: utf-8

##########################################################################
# Problem statement: Identify suspicious financial transactions         ##
# Approach: Factors considered -                                        ##
#              (1) Daily input volume of transactions                   ##
#              (2) Overall invalid transactions                         ##
#              (3) Parameters to be passed                              ##    
#              (4) Scalability from batch to near real time             ##    
#              (5) Other factors like cluster sizing, multi-processing  ##
##########################################################################

# Import some basic libraries
from datetime import date, timedelta
import datetime
from pyspark import SparkContext
import pandas as pd
import sys

spark = SparkSession.builder.appName('AML_Suspicious_Transactions_Application').getOrCreate()
   
def process_write():
    #############################################################################################
    # Two possible scenarios -
    # 1. Multiple transactions can happen between A & C amount. (Considered this possibility)
    #    First transaction with amount to be transferred to B.
    #    Second transaction with just a fee amount to C.
    # 2. Single transaction between A & C and C & B transfer will be 0 - 10% variation    
    #############################################################################################

    transactions=transactions.rdd.map(lambda t: (t.receiver, (t.transaction, t.timestamp, t.amount, t.sender, t.receiver))).join(transactions.rdd.map(lambda u: (u.sender,
                        (u.transaction, u.timestamp, u.amount, u.sender, u.receiver)))) \
                .filter(lambda x: x[1][0][1]<=x[1][1][1]<=x[1][0][1]+ str(timedelta(days=day))
                        and (float((x[1][0][2]))*(1.0-maxfrac)<=float(x[1][1][2])<=float((x[1][0][2]))*(1.0-minfrac)))\
                .flatMap(lambda x:  (x[1][0],x[1][1]))\
                .distinct()\
                .cache()
                   
    # Final step - Write transactions to csv
    valtransactions=transactions.collect()
    df = pd.DataFrame(valtransactions)

    # Clean-up the data from given transaction file as data contains transactions with future dated.
    # This can be done once the input data has been read but to understand if there are any suspicous transactions
    # but reported as incorrect

    dt_minus3 = (datetime.now() - timedelta(days=days_to_subtract)).strftime("%Y-%m-%d %H:%M:%S")
    df = df[(df['timestamp'] <= dt_now) & (df['timestamp'] > dt_minus3)]
    df.to_csv("/mapr/D00NSVL-DCMPR01.sddev.lpl.com/focus/inbound/suspicioustransactions.csv")      

    persons=sorted(transactions.flatMap(lambda x: [(x[3],1),(x[4],1)]).reduceByKey(lambda x,y: x+y).collect(),key=lambda x: -x[1])
    df_susp_per = pd.DataFrame(persons)
    df_susp_per.to_csv("/mapr/D00NSVL-DCMPR01.sddev.lpl.com/focus/inbound/suspiciouspersons.csv")

if __name__=='__main__':  

#############################################################################################
#User inputs: 1. # of days to be  considered for bridging transactions
#             2. Minimum fraction value and Max fraction to be considered as commission                
#############################################################################################

    bridging_days = sys.argv[1]
    day=int(bridging_days)
    print("Passed on number of days identifying bridging transactions:",day)
       
    min_bridge_val = sys.argv[2]
    minfrac=float(min_bridge_val)
    print("Passed on minimum float value to be considered:", min_bridge_val)
       
    max_bridge_val = sys.argv[3]
    print("Passed on maximum float value to be considered:",max_bridge_val)

    if max_bridge_val < min_bridge_val or max_bridge_val =0:
        print('Invalid bridge values entered')
       
    #create the base transactions set

    transactions = spark.read.csv("transactions_kaggle.csv", sep ='|', header = True)
    transactions.rdd.filter(lambda x: x.sender != x.receiver)
    try:
      process_write()
      print("Process successfully complete)
    except:
      print("An exception occurred")
