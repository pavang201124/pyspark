Problem statement: Identify suspicious financial transactions 
https://bitbucket.org/quantaverse/softeng_test/src/master/		

Approach: Factors considered -                                        
              (1) Daily input volume of transactions                   
              (2) Overall invalid transactions                         
              (3) Parameters to be passed                                  
              (4) Scalability from batch to near real time                 
              (5) Other factors like cluster sizing, multi-processing  

Two possible scenarios -
   	1. Multiple transactions can happen between A & C amount. (Considered this possibility)
    	First transaction with amount to be transferred to B.
    	Second transaction with just a fee amount to C.
    2. Single transaction between A & C and C & B transfer will be 0 - 10% variation    

Input parameters:
	User inputs: 1. # of days to be  considered for bridging transactions
	             2. Minimum fraction value and Max fraction to be considered as commission  

Pre-requisites for code to be executed:
	1. Python 3.5 version to be installed on Spark cluster
	2. All basic libraries that come with base version of Python. 
	3. Pandas library
	
Steps to execute the code:
	spark-submit --master local --deploy-mode cluster --driver-memory 2g --executor-memory 2g aml_transaction_identifier.py 3 0.01 0.1

Output:
	suspicious_transactions.csv
    
Future Enhancements: 
  1. Use multiprocessing and memory profiler within spark code to improve worker threads performance
  2. Use joblib, dask to improve performance
  3. Plot visualization to understand overall trend of these kind of transactions
